<?php

class blog_Model
    {
    private $db;
    private $table = 'blog';
    public function __construct()
        {
        $this->db = new Database;
        }
    public function get_all()
        {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
        }
    public function get_byId($id)
        {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=' . $id);
        // $this->db->bind('id', $id);
        return $this->db->single();
        }
    public function delete_byId($id)
        {
        $this->db->query('DELETE FROM ' . $this->table . ' WHERE id=' . $id);
        return $this->db->single();
        }
    public function delete_Img_byId($id)
        {
        $queryGambar = "SELECT * FROM blog WHERE id = :id";
        $this->db->query($queryGambar);
        $this->db->bind('id', $id);
        $result = $this->db->single();
        $imageName = $result['img'];

        $imagePath = "../public/uploads/" . $imageName;
        if (file_exists($imagePath)) {
            unlink($imagePath);
            }

        $query = "DELETE FROM $this->table WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();

        return $this->db->rowCount();
        }
    public function create_Item($data)
        {
        $query = "INSERT INTO $this->table(img,judul,kategori,desk) VALUES (:img,:judul,:kategori,:desk)";
        $this->db->query($query);
        $this->db->bind(':judul', $data['judul']);
        $this->db->bind(':img', $data['img']);
        $this->db->bind(':kategori', $data['kategori']);
        $this->db->bind(':desk', $data['desk']);
        $this->db->execute();
        return $this->db->rowCount();
        }
    public function update_Item($data)
        {
        $imgName = $_FILES['image']['name'];
        $imgTmp = $_FILES['image']['tmp_name'];
        $allowExt = ['jpeg', 'jpg', 'png'];
        $imgExt = pathinfo($imgName, PATHINFO_EXTENSION);

        if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $oldImg = $imgName;

            if (in_array($imgExt, $allowExt)) {
                $uniqueImg = time() . '-' . $imgName;
                $directory = "../uploads/";
                $targetPath = $directory . $uniqueImg;

                if (move_uploaded_file($imgTmp, $targetPath)) {
                    $oldImgPath = $directory . $oldImg;
                    if (file_exists($oldImgPath)) {
                        unlink($oldImgPath);
                        }

                    $query = "UPDATE $this->table SET img=:img, judul=:judul, kategori=:kategori, desk=:desk WHERE id=:id";
                    $this->db->query($query);
                    $this->db->bind('id', $data['id']);
                    $this->db->bind('img', $uniqueImg);
                    $this->db->bind('judul', $data['judul']);
                    $this->db->bind('kategori', $data['kategori']);
                    $this->db->bind('desk', $data['desk']);
                    $this->db->execute();
                    return $this->db->rowCount();
                    }
                }
            else {
                $query = "UPDATE $this->table SET judul=:judul, kategori=:kategori, desk=:desk WHERE id=:id";
                $this->db->query($query);
                $this->db->bind('id', $data['id']);
                $this->db->bind('judul', $data['judul']);
                $this->db->bind('kategori', $data['kategori']);
                $this->db->bind('desk', $data['desk']);
                $this->db->execute();
                return $this->db->rowCount();
                }
            return $this->db->rowCount();
            }
        }

    public function searchId()
        {
        $keyword = $_POST['keyword'];
        $query = "SELECT * FROM $this->table WHERE judul LIKE :keyword";
        $this->db->query($query);
        $this->db->bind('keyword', $keyword);
        return $this->db->resultSet();
        }
    }