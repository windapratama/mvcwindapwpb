<?php

class About extends Controller
    {

    public function index()
        {
        $data['judul'] = 'about';
        $data['test'] = $this->model('blog_model')->get_all();
        $this->view('templates/header', $data);
        $this->view('templates/navbar');
        $this->view('home/about', $data);
        $this->view('templates/footer');
        }

    public function detail($id)
        {
        $data['judul'] = 'detail';
        $data['test'] = $this->model('blog_model')->get_byId($id);
        $this->view('templates/header', $data);
        $this->view('templates/navbar');
        $this->view('home/detail', $data);
        $this->view('templates/footer');
        }
    public function delete($id)
        {
        Flasher::setFlash('Data Berhasil Dihapus', 'Benar.', 'success');
        $this->model('blog_Model')->delete_Img_byId($id);
        $this->model('blog_model')->delete_byId($id);
        header('Location:' . BASEURL . '/About');
        }
    public function create()
        {
        $path = $this->uploadImage($_FILES['img']);
        $_POST['img'] = $path;

        if ($this->model('blog_Model')->create_Item($_POST) > 0) {
            Flasher::setFlash('Berhasil Dimasukan', 'Benar.', 'success');
            header('Location:' . BASEURL . '/About');
            exit;
            }
        else {
            header('Location:' . BASEURL . '/About');
            Flasher::setFlash('Gagal Dimasukan', 'Salah.', 'danger');
            exit;
            }
        }

    private function uploadImage($file)
        {
        $uploadDirectory = '../public/uploads/';
        $filename = uniqid() . '_' . basename($file['name']);
        $targetPath = $uploadDirectory . $filename;

        if (move_uploaded_file($file['tmp_name'], $targetPath)) {
            return $filename;
            }
        else {
            return false;
            }
        }

    public function update()
        {
        if ($this->model('blog_model')->update_Item($_POST) > 0) {
            header('Location: ' . BASEURL . '/About');
            exit;
            }
        else {
            header('Location: ' . BASEURL . '/About');
            }
        }
    private function updateImage($image, $file)
        {
        if ($_FILES['img']['tmp_name']) {
            if ($file['img'] != null) {
                unlink('../public/uploads/' . $file['img']);
                }

            $uploadDirectory = 'path/to/storage/public/img';
            $pathImage = $_FILES['img']['tmp_name'];
            $filename = $_FILES['img']['name'];
            $path = $uploadDirectory . '/' . $filename;

            if (move_uploaded_file($pathImage, $path)) {
                return $path;
                }
            }
        else {
            $path = $file['img'];
            }
        }

    public function json()
        {
        echo json_encode($this->model('blog_Model')->get_byId($_POST['id']));
        }

    public function cari()
        {
        $data['judul'] = 'about';
        $data['test'] = $this->model('blog_model')->searchId();
        $this->view('templates/header', $data);
        $this->view('templates/navbar');
        $this->view('home/about', $data);
        $this->view('templates/footer');
        }
    }