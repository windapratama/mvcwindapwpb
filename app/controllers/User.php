<?php
class user extends Controller
    {

    public function Dashboard()
        {
        if (! $_SESSION['username']) {
            header('location: ' . BASEURL . '/Auth/Login');
            }
        if ($_SESSION['username']) {
            $data['test'] = $_SESSION['username'];
            }
        $data['judul'] = 'detail';
        $this->view('templates/header', $data);
        $this->view('templates/navbar');
        $this->view('dashboard/index', $data);
        $this->view('templates/footer');
        }
    public function Detail()
        {
        if (! $_SESSION['username']) {
            header('location: ' . BASEURL . '/Auth/Login');
            }
        if ($_SESSION['username']) {
            $data['test'] = $_SESSION['username'];
            }
        $data['judul'] = 'detail';
        $data['test'] = $this->model('user_Model')->get_all();
        $this->view('templates/header', $data);
        $this->view('templates/navbar');
        $this->view('dashboard/listAccount', $data);
        $this->view('templates/footer');
        }

    public function delete($id)
        {
        Flasher::setFlash('Data Berhasil Dihapus', 'Benar.', 'success');
        $this->model('user_model')->delete_byId($id);
        header('Location:' . BASEURL . '/user/detail');
        }

    public function json()
        {
        echo json_encode($this->model('user_Model')->get_byId($_POST['id']));
        }
    }