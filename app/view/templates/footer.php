</div>
<script src="<?= BASEURL ?>/js/bootstrap.min.js"></script>
<script src="<?= BASEURL ?>/jquery/script.js"></script>

<script>
$(document).ready(function() {
    $('#img').change(function() {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result).show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#img').on('click', function() {
        $('#preview').attr('src', '').hide();
        $(this).val('');
    });
});
</script>
<script>
$(document).ready(function() {
    $('.updateModal').on('click', function() {
        $('#Label_Modal').html('Update Data');

        $('.modal-footer button[type=submit]').html('Update');

        $('.modal-body form').attr(
            'action',
            'http://localhost/mvc/public/about/update'
        );

        const id = $(this).data('id');
        console.log(id)

        $.ajax({
            url: 'http://localhost/mvc/public/about/json',
            data: {
                id: id
            },
            method: "post",
            dataType: "json",
            success: function(data) {
                console.log(data);
                var BaseUrl = 'http://localhost/mvc/public/uploads/'
                var ImgNew = BaseUrl + data.img;
                $('#id').val(data.id);
                $('#judul').val(data.judul);
                $('#kategori').val(data.kategori);
                $('#desk').val(data.desk);
                $('#preview').attr('src', ImgNew);
            },
        });
    });
});
$('.add-item').on('click', function() {
    $('#Label_Modal').html('Add Data');
    $('.modal-footer button[type=submit]').html('Submit');
});
</script>
<script>
$(document).ready(function() {
    $('.updateUser').on('click', function() {
        $('#Label_Modal').html('Update Data');

        $('.modal-footer button[type=submit]').html('Update');

        $('.modal-body form').attr(
            'action',
            'http://localhost/mvc/public/user/update'
        );

        const id = $(this).data('id');
        console.log(id)

        $.ajax({
            url: 'http://localhost/mvc/public/user/json',
            data: {
                id: id
            },
            method: "post",
            dataType: "json",
            success: function(data) {
                console.log(data);
                $('#id').val(data.id);
                $('#judul').val(data.judul);
                $('#kategori').val(data.kategori);
                $('#desk').val(data.desk);
            },
        });
    });
});
$('.add-item').on('click', function() {
    $('#Label_Modal').html('Add Data');
    $('.modal-footer button[type=submit]').html('Submit');
});
</script>
</body>

</html>