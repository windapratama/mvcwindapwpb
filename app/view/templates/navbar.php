<?php
$helper = new Helper;
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="<?= $url = $helper->url(''); ?>">Winda Pratama</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link" href="<?= $url = $helper->url(''); ?>">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $url = $helper->url('about'); ?>">About</a>
                </li>
                <?php if (! isset($_SESSION['session_login'])) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $url = $helper->url('Auth/Login'); ?>">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $url = $helper->url('Auth/Register'); ?>">Register</a>
                </li>
                <?php }
                else { ?>
                <li class="nav-item">
                    <a class="nav-link"
                        href="<?= $url = $helper->url('user/dashboard'); ?>"><?php echo $_SESSION['username']; ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $url = $helper->url('auth/logout'); ?>">Logout</a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>