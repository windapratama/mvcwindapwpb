<?php
$helper = new Helper();
Flasher::flash();
?>

<!-- <style>
    .bg-dark {
        background-color: #6091c3 !important;
} -->
</style>
<section class="vh-100" id="form">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="card bg-dark text-white" style="border-radius: 0.5rem; background-color: #6091c3 !important;">
                    <div class="card-body p-5 mt-3">
                        <h2 class="fw-bold mb-2 text-uppercase">Login</h2>
                        <?php Flasher::flash() ?>
                        <form class="mb-md-5 mt-md-4 pb-5" action="<?= $helper->url('Auth/LogProses') ?>" method="POST">
                            <div class="form-outline form-white mb-4">
                                <label class="form-label" for="typeEmailX">Username</label>
                                <input type="text" id="typeEmailX" class="form-control form-control-lg"
                                    name="username" />
                            </div>

                            <div class="form-outline form-white mb-4">
                                <label class="form-label" for="typePasswordX">Password</label>
                                <input type="password" id="typePasswordX" class="form-control form-control-lg"
                                    name="password" />
                            </div>

                            <p class="small mb-5 pb-lg-2 text-center"><a class="text-white-50" href="#!">Forgot
                                    password?</a></p>
                            <div class="text-center">
                                <button class="btn btn-outline-light btn-lg px-5" type="submit">Login</button>
                            </div>
                        </form>

                        <div>
                            <p class="mb-0 text-center">Don't have an account? <a
                                    href="<?= $helper->url('Auth/Register') ?>"
                                    class="text-white-50 fw-bold">Register</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>