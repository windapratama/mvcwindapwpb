<?php
$helper = new helper();
Flasher::flash();
?>
<section class="vh-100" id="form">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="card bg-dark text-white" style="border-radius: 0.5rem; background-color: #6091c3 !important;">
                    <div class="card-body p-5 mt-3">
                        <h2 class="fw-bold mb-2 text-uppercase">Register</h2>
                        <form class="mb-md-5 mt-md-4 pb-5" action="<?= $helper->url('Auth/RegProses') ?>" method="POST">
                            <?php Flasher::flash(); ?>

                            <div class="form-outline form-white mb-4">
                                <label class="form-label" for="typeEmailX">Username</label>
                                <input type="text" id="typeEmailX" class="form-control form-control-lg" name="username"
                                    required />
                            </div>
                            <input type="text" class="form-control form-control-lg" name="img" hidden value="w1n.jpg" />
                            <div class="form-outline form-white mb-4">
                                <label class="form-label" for="typePasswordX">Password</label>
                                <input type="password" id="typePasswordX" class="form-control form-control-lg"
                                    name="password" required />
                            </div>
                            <div class="form-outline form-white mb-4">
                                <label class="form-label" for="typeRoleX">Role</label>
                                <div class="form-outline">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="role" id="flexRadioDefault2"
                                            checked value="user">
                                        <label class="form-check-label" for="flexRadioDefault2">User</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="role" id="flexRadioDefault1"
                                            value="admin">
                                        <label class="form-check-label" for="flexRadioDefault1">Admin</label>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <button class="btn btn-outline-light btn-lg px-5" type="submit">Submit</button>
                            </div>
                        </form>
                        <div>
                            <p class="mb-0 text-center">Have account?<a href="<?= $helper->url('Auth/login') ?>"
                                    class="text-white-50 fw-bold">login</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>