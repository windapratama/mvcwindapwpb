<style>
@supports (-webkit-backdrop-filter: none) or (backdrop-filter: none) {
    .jumbotron {
        -webkit-backdrop-filter: blur(3px);
        backdrop-filter: blur(3px);
        background-color: rgba(0, 0, 0, 0.5);
    }

    .warning {
        display: none;
    }
}

.jumbotron {
    position: absolute;
    top: 45%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 70%;
    border-radius: 15px;
}

.jumbotron h1,
.jumbotron p,
.jumbotron a {
    color: white;
}

.bg-black {
    background-color: #6091c3 ;
}
</style>


<div class="jumbotron border mt-5 p-5 bg-glass container bg-black">
    <h1 class="display-6 text-light fw-medium mb-4">Hii, Welcome!</h1>
    <p class="lead text-light">Saya Winda</p>
    <hr class="my-4 text-light border-2">
    <p class="text-light">Ni Putu Winda Pratama Putri (XII RPL 1 / 34).</p>
    <a class="btn btn-outline-light" href="<?= BASEURL . "/about" ?>">Learn More</a>
</div>
