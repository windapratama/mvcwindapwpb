<style>
.card {
    width: auto;
    overflow: hidden;
}

.img {
    width: auto;
    height: 325px;
}
</style>

<?php
Flasher::flash();
$helper = new Helper();
?>

<div class="container">
    <div class="container mt-3">
        <div class="row">
            <div class="col-6">
                <button type="button" class="btn btn-primary mt-3 add-item" data-bs-toggle="modal"
                    data-bs-target="#formModal">
                    Add Item
                </button>
            </div>
            <div class="col-6">
                <div class="input-group">
                    <form action="<?= $helper->url('About/cari') ?>" method="POST" class="w-100 d-flex">
                        <input type="text" class="form-control" placeholder="search " aria-label="search "
                            aria-describedby="button-addon2" autocomplete="off" name="keyword" id="keyword">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">SEARCH</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="row mt-5">
        <?php foreach ($data['test'] as $blog): ?>
        <div class="col-lg-4">
            <div class="card mb-3">
                <div class="card-body text-center">
                    <img class="card-img-top" src="<?= BASEURL ?>/uploads/<?= $blog['img'] ?>" alt="Card image">
                    <h5 class="card-title"><?= $blog['judul'] ?></h5>
                    <p class="card-text"><?= $blog['desk'] ?></p>
                    <a href="<?= $url = $helper->url('About/detail/' . $blog['id']); ?>"
                        class="btn btn-primary">DETAILS</a>
                    <a href="<?= $url = $helper->url('About/delete/' . $blog['id']); ?>"
                        class="btn btn-danger">DELETE</a>
                    <a href="<?= $url = $helper->url('About/update/' . $blog['id']); ?>"
                        class="btn btn-success updateModal" data-bs-toggle="modal" data-bs-target="#formModal"
                        data-id="<?= $blog['id'] ?>">UPDATE</a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<div class=" modal fade" id="formModal" tabindex="-1" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Label_Modal">Create Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= $helper->url('About/create') ?>" method="POST" enctype="multipart/form-data">
                    <input type="text" hidden id="id" name="id">
                    <div class="mb-3">
                        <label for="judul">Title</label>
                        <input type="text" id="judul" class="form-control" name="judul" required>
                    </div>
                    <div class="mb-3">
                        <label for="image">Image</label>
                        <input type="file" id="image" class="form-control" name="img">
                        <img src="" id="preview" alt="" class="img-fluid mt-3">
                    </div>
                    <div class="mb-3">
                        <label for="kategori">Category</label>
                        <input type="text" id="kategori" class="form-control" name="kategori" required>
                    </div>
                    <div class="mb-3">
                        <label for="desk">Description</label>
                        <input type="text" id="desk" class="form-control" name="desk" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>